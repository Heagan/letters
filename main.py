from PIL import Image
import sys
import numpy as np


from rfr import *

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

def getcol(code):
	blue =  code & 255
	green = (code >> 8) & 255
	red =   (code >> 16) & 255
	return (red, green, blue)

def mkcol(red, green, blue):
	return red * 256 * 256 + green * 256 + blue

def mkcolt(tuple):
	return tuple[0] * 256 * 256 + tuple[1] * 256 + tuple[2]

def process(FILE):
	try:
		im = Image.open(FILE)
	except:
		print("Unable to open file: " + FILE)
		print("Make sure to include picture extension, eg 'example.png', 'example.jpg'")
		exit()

	# get rid of alpha
	im = im.convert('RGB')
	pix = im.load()
	# get sum of all pixel colours
	pc = 0
	for x in range(im.width):
		for y in range(im.height):
			pc += mkcolt(pix[x, y])

	# (sum of colours added) / white * (max number of pixels)
	avg = pc / (im.width * im.height * mkcol(255, 255, 255))
	# use average to get the equivalent colour value
	# 15% threshhold, so if its 15% darker it'll still count
	thresh= round( mkcol(255, 255, 255) * avg / 1.15) 

	l = []

	pix = im.load()
	for x in range(im.width):
		for y in range(im.height):
			pc = mkcolt(pix[x, y])
			pix[x, y] = WHITE if pc > thresh else BLACK
			l.append(mkcolt(pix[x, y]))
	# n = 0
	# middle = (0, 0)
	# for x in range(im.width):
	# 	for y in range(im.height):
	# 		if pix[x, y] == BLACK:
	# 			middle = ( (middle[0] + x), (middle[1] + y) )
	# 			n += 1

	# if middle is not (0, 0):
	# 	middle = ( round(middle[0] / n), round(middle[1] / n) )
	# 	pix[middle[0], middle[1]] = (255, 0, 0)
	
	# quad1, quad2, quad3, quad4 = 0, 0, 0, 0
	# for x in range(im.width):
	# 	for y in range(im.height):
	# 		if pix[x, y] == BLACK:
	# 			if x < middle[0]:
	# 				if y < middle[1]:
	# 					quad1 += 1
	# 				else:
	# 					quad2 += 1
	# 			else:
	# 				if y < middle[1]:
	# 					quad3 += 1
	# 				else:
	# 					quad4 += 1
	l = np.array(l).reshape(1, -1)
	# print(l)
	# exit()

	answer = input("What letter is the image: ")
	if len(answer) is 0:
		print("I predict the picture is the letter ", chr(predict([l]) ) )
	else:
		train(l, [ord(answer)])
		print("I predict the picture is the letter ", chr(predict(l) ) )

	save_model()

	im.save("ass.png")


if __name__ == '__main__':
	if input("Load model? [y/n] ") == 'y':
		load_model()
		print("Loaded model")
	else:
		setup_ml(n_estimators=1)
	while True:
		FILE = input("Enter filename of picture: ") + '.png'
		if FILE == '.png':
			break
		process(FILE)















