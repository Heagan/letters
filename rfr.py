import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import StandardScaler

import pickle

regressor = None
sc_X = None

training_x = []
training_y = []

def setup_ml(n_estimators = 5):
	global regressor, sc_X

	print("Setting up network")
	sc_X = StandardScaler()

	regressor = RandomForestRegressor(n_estimators = n_estimators)

	print("Done! Ready to predict!")


def train(inputs: [], answers: []):
	global regressor, sc_X, training_x, training_y
	X_train = sc_X.fit_transform(inputs)
	
	for x in X_train:
		training_x.append(x)
	for y in answers:
		training_y.append(y)
	
	regressor.fit(training_x, training_y)


def predict(X):
	global regressor, sc_X
	
	X = sc_X.transform(X)
	return regressor.predict(X)


def save_model():
	global regressor, sc_X

	pickle.dump(regressor, open('brain.p', 'wb'))
	pickle.dump(sc_X, open('sampler.p', 'wb'))
	

def load_model():
	global regressor, sc_X

	regressor = pickle.load( open( "brain.p", "rb" ) )
	sc_X = pickle.load( open( "sampler.p", "rb" ) )



# setup_ml([[0, 0], [1, 1]], [0, 0], 1)

# while True:
# 	p = predict([[1, 1]])
# 	print( p, end='' )
# 	if round(p[0]) == 1:
# 		break
# 	print(" attept to improve model")
# 	train([[1, 1]], [1])

# p = predict([[0, 0]])
# print( p )


# save_model()
# load_model()