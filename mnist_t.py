
from knearest import Brain
brain = Brain(784, 2)
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'


from mnist import MNIST

mndata = MNIST('./datafiles')
images, labels = mndata.load_training()

train_range_1 = 0
train_range_2 = 8000

print(f"Training brain with '{train_range_2 - train_range_1}' values")
for i in range(train_range_1, train_range_2):
    brain.add(images[i], labels[i])
print("Training Done")

brain.save_brain("brain.sav")
brain.load_brain("brain.sav")
# exit()
predict_range_1 = 9940
predict_range_2 = 10000

print(f"Predicting '{predict_range_2 - predict_range_1}' values")

# breakpoint()
correct = 0
count = 0
for i in range(predict_range_1, predict_range_2):
    count += 1
    prediction = round(brain.predict(images[i]))
    correct += 1 if prediction == labels[i] else 0
    COLOUR = GREEN if prediction == labels[i] else RED
    print(f"{COLOUR}Prediction {i}: '{prediction}' correct '{labels[i]}'\t\t{round(correct/count* 100)}% {correct}/{count}{NC}")

print("Prediction Completed!")
print(f"Accuracy {correct}/{predict_range_2 - predict_range_1}")

breakpoint()

